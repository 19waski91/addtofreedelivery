<?php

include_once (_PS_MODULE_DIR_.'addtofreedelivery/classes/FreeShippingProducts.php');
if (!defined('_PS_VERSION_'))
    exit;

class AddToFreeDelivery extends Module
{
    public function __construct()
    {
        $this->name = 'addtofreedelivery';
        $this->tab = 'other';
        $this->version = '1.1.0';
        $this->author = 'Łukasz Machowski';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Add to free delivery');
        $this->description = $this->l('Add to free delivery');
        $this->context->smarty->assign('module_name', $this->name);
        $this->confirmUninstall = $this->l('Czy na pewno chcesz odinstalować?');
        $this->ps_versions_compliancy = array('min' => '1.6.1.0', 'max' => _PS_VERSION_);

        if (!Configuration::get('addtofreedelivery'))
            $this->warning = $this->l('No name provided');
    }

    public function hookShoppingCart()
    {
        $quantity = $display_product = Configuration::get('PS_DISPLAY_PRODUCT');
        $margin_price_percent = Configuration::get('PS_MARGIN_PRICE_PERCENT');

        $productsSet = Configuration::get('PS_PRODUCTS_SELECT');
        if (empty($productsSet)){
            $productsSet = [];
        }else{
            $productsSet = explode('-', $productsSet);
        }

        $categorySet = Configuration::get('PS_CATEGORY_SELECT');
        if (empty($categorySet)){
            $categorySet = [];
        }else{
            $categorySet = explode('-', $categorySet);
        }

        $shippingModel = new FreeShippingProducts();
        $totalFreeShipping = $shippingModel->getTotalFreeShipping();
        $totalFreeShippingMargin = $totalFreeShipping;
        $totalFreeShippingMin = $totalFreeShipping;
        if (isset($margin_price_percent) && $margin_price_percent){
            $totalFreeShippingMargin = $totalFreeShipping *  ((100 + $margin_price_percent) / 100);
        }

        $products = $shippingModel->getProducts($totalFreeShippingMin, $totalFreeShippingMargin, $quantity, $categorySet, $productsSet);

        $this->context->smarty->assign([
            'priceToFreeDelivery' => $totalFreeShipping,
            'freeShippingProductListDir' => dirname(__FILE__),
        ]);

        if (_PS_VERSION_ >= '1.7.0'){

            $this->context->smarty->assign([
                'freeShippingProducts' => isset($products) && !empty($products) ? array_map(array($this, 'prepareProductForTemplate'), $products) : null,
            ]);

            return $this->fetch('module:addtofreedelivery/views/templates/freeshippingproduct17.tpl');
        }else{

            $this->context->smarty->assign([
                'freeShippingProducts' => $products,
            ]);
            return $this->display(__FILE__, 'views/templates/freeshippingproducts.tpl');
        }
    }

    private function prepareProductForTemplate(array $rawProduct)
    {
        $product = (new ProductAssembler($this->context))
            ->assembleProduct($rawProduct)
        ;

        $presenter = $this->getProductPresenter();
        $settings = $this->getProductPresentationSettings();

        return $presenter->present(
            $settings,
            $product,
            $this->context->language
        );
    }

    private function getFactory()
    {
        return new ProductPresenterFactory($this->context, new TaxConfiguration());
    }

    protected function getProductPresentationSettings()
    {
        return $this->getFactory()->getPresentationSettings();
    }

    protected function getProductPresenter()
    {
        return $this->getFactory()->getPresenter();
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
//        $this->context->controller->addCSS(_PS_MODULE_DIR_.'addtofreedelivery/views/css/product_list.css');
//        $this->context->controller->addJS(_PS_MODULE_DIR_.'addtofreedelivery/views/js/freeshipping.js');
    }

    public function getContent()
    {
        $message = '';
        if (Tools::isSubmit('saveAddTofreeDelivery')){
            Configuration::updateGlobalValue('PS_CATEGORY_SELECT', implode('-', Tools::getValue('categoryBox', [])));
            Configuration::updateGlobalValue('PS_PRODUCTS_SELECT', Tools::getValue('inputAccessories--autocomplete_add_to_free_delivery', ''));

            $free_delivery = Tools::getValue('free_delivery', 0);

            if (!ValidateCore::isUnsignedFloat($free_delivery)){
                $message .= $this->displayError($this->l('Free delivery has incorrect value'));
            }else{
                Configuration::updateGlobalValue('PS_SHIPPING_FREE_PRICE', $free_delivery);
            }

            $margin_price_percent = Tools::getValue('margin_price_percent', 0);

            if (!ValidateCore::isUnsignedInt($margin_price_percent)){
                $message .= $this->displayError($this->l('Margin price percent has incorrect value'));
            }else{
                Configuration::updateGlobalValue('PS_MARGIN_PRICE_PERCENT', $margin_price_percent);
            }

            $display_product = Tools::getValue('display_product', 0);

            if (!ValidateCore::isUnsignedInt($display_product)){
                $message .= $this->displayError($this->l('Display product has incorrect value'));
            }else{
                Configuration::updateGlobalValue('PS_DISPLAY_PRODUCT', $display_product);
            }
        }
        return $this->renderForm($message);
    }

    public function renderForm($message)
    {
        $categorySet = Configuration::get('PS_CATEGORY_SELECT');

        if (empty($categorySet)){
            $categorySet = [];
        }else{
            $categorySet = explode('-', $categorySet);
        }

        $tree_categories_helper = new HelperTreeCategories('categories-treeview');
        $tree_categories_helper
            ->setRootCategory(Category::getRootCategory()->id_category)
            ->setUseCheckBox(true)
            ->setSelectedCategories($categorySet);

        $free_delivery = Configuration::get('PS_SHIPPING_FREE_PRICE');
        $margin_price_percent = Configuration::get('PS_MARGIN_PRICE_PERCENT');
        $display_product = Configuration::get('PS_DISPLAY_PRODUCT');

        $this->context->smarty->assign(array(
                                            'message' => $message,
                                            'categories_tree' => $tree_categories_helper->render(),
                                            'free_delivery' => $free_delivery ? $free_delivery : 0,
                                            'margin_price_percent' => $margin_price_percent ? $margin_price_percent : 0,
                                            'display_product' => $display_product ? $display_product : 0,
                                            'current_url' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
                                            'autocomplete' => $this->generateAutocomplete(),
                                            )
                                        );

        return $this->display(__FILE__, 'views/templates/admin/form.tpl');
    }

    private function generateAutocomplete(){

        $linkToActionGetAllCategory = $this->context->link->getAdminLink('AdminAddToFreeDelivery').'&action=GetAllProducts&ajax=true';

        $this->context->smarty->assign(array(

                'autocompletes' => array(array('name' => 'autocomplete_add_to_free_delivery',
                                        'label' => '',
                                        'attributes' => $this->getProductsAssign(),
                                        'prefix' => '-',
                                        'url' => $linkToActionGetAllCategory,
                                        'limit' => 100,
                                        )),
                'srcs' => array(
                                str_replace($_SERVER['DOCUMENT_ROOT'],'',_PS_MODULE_DIR_).'/addtofreedelivery/'.'views/js/autocomplete.js',
                                ),

                'div' => false,
                'name' => 'autocomplete',
                'form' => ['flag' => false, 'src' => '', 'ajax' => 'false'],
            )
        );

        return $this->display(__FILE__, 'views/templates/admin/autocomplete.tpl');
    }

    private function getProductsAssign(){

        $productsSet = Configuration::get('PS_PRODUCTS_SELECT');

        if (empty($productsSet)){
            return null;
        }

        $productsSet = explode('-', $productsSet);


        foreach ($productsSet as $productId){
            if (isset($productId) && $productId){
                $tmp[] = ['id' => $productId,
                    'name' => Product::getProductName($productId)];
            }
        }

        return $tmp;
    }

    public function install()
    {
        return parent::install()
            && $this->registerHooks()
            && $this->installSqls();
    }

    public function registerHooks()
    {
        $return = true;
        $return &= $this->registerHook('shoppingCart');
        $return &= $this->registerHook('header');
        return $return;
    }

    public function installSqls()
    {
        $sqls = array();
        $result = true;
        $db = Db::getInstance();

        $isTable = $db->getValue('SELECT id_tab FROM '._DB_PREFIX_.'tab WHERE class_name LIKE "AdminAddToFreeDelivery";');

        if(!$isTable){
            $result &= $db->insert('tab', array('id_parent' => -1,
                'class_name' => 'AdminAddToFreeDelivery',
                'module' => 'addtofreedelivery',
                'position' => 0,
                'active' => 1,
                'hide_host_mode' => 0));
        }

        foreach ($sqls as $sql)
            $result &= $db->execute($sql);

        return $result;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallDb())
            return false;

        return true;
    }

    private function uninstallDb()
    {
        $sqls = array();
        $db = Db::getInstance();

        $result = true;
        foreach ($sqls as $sql)
            $result &= $db->execute($sql);

        return $result;
    }
}