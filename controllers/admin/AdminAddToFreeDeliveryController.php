<?php

class AdminAddToFreeDeliveryController extends ModuleAdminController
{
    public function displayAjaxGetAllProducts()
    {
        if (!defined('_PS_ADMIN_DIR_')) {
            define('_PS_ADMIN_DIR_', getcwd());
        }

        $query = Tools::getValue('q', false);
        if (!$query or $query == '' or strlen($query) < 1) {
            die();
        }

        if ($pos = strpos($query, ' (ref:')) {
            $query = substr($query, 0, $pos);
        }

        $excludeIds = Tools::getValue('excludeIds', false);
        if ($excludeIds && $excludeIds != 'NaN') {
            $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
        } else {
            $excludeIds = '';
        }

        $items = $this->getProductsAutocomplete($query, $excludeIds);

            if (!empty($items)) {
                foreach ($items as $item) {
                    echo trim($item['name']).'|'.(int)($item['id_product'])."\n";
                }
                die();
            }

        Tools::jsonEncode(new stdClass);
    }


    public function getProductsAutocomplete($query, $excludeIds)
    {
        $query = pSQL($query);
        $langId = Context::getContext()->language->id;
        $shopId = Context::getContext()->shop->id;

        $sql = new DbQuery();
        $sql->select('p.id_product, pl.name');
        $sql->from('product', 'p');
        $sql->innerJoin('product_lang', 'pl', 'p.id_product = pl.id_product');
        $sql->where('pl.id_lang = '.(int)$langId);
        $sql->where('pl.id_shop = '.(int)$shopId);
        $sql->where('pl.name LIKE "%'.$query.'%"');
        $sql->where('p.active = 1');
        !empty($excludeIds)?  $sql->where('p.id_product NOT IN ('.$excludeIds.')') : '';
        return Db::getInstance()->executeS($sql);
    }
}