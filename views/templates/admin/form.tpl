{if isset($message)}{$message}{/if}
<div class="panel">
	<h3><i class="icon-cogs"></i> {l s='Add to free delivery configuration' mod='addtofreedelivery'}</h3>
	<form action="{$current_url}" method="post" class="form-horizontal" onsubmit="return checkForm();">
		<div class="form-group">
			<label class="control-label col-lg-3">{l s='Categories used:' mod='addtofreedelivery'}</label>
			<div class="col-lg-9">
				{if trim($categories_tree) != ''}
					{$categories_tree}
				{else}
					<div class="alert alert-warning">
						{l s='Categories selection is disabled because you have no categories or you are in a "all shops" context.' mod='addtofreedelivery'}
					</div>
				{/if}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">{l s='Products used:' mod='addtofreedelivery'}</label>
			<div class="col-lg-9">
                {if isset($autocomplete)}
                    {$autocomplete}

                {/if}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">
                {l s='Free delivery:' mod='addtofreedelivery'}
			</label>
			<div class="col-lg-9">
				<input type="text" name="free_delivery" id="free_delivery" value="{if isset($free_delivery)}{$free_delivery}{/if}" class="">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3">
                {l s='Quantity display product:' mod='addtofreedelivery'}
			</label>
			<div class="col-lg-9">
				<input type="text" name="display_product" id="display_product" value="{if isset($display_product)}{$display_product}{/if}" class="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">
                {l s='Margin price percent:' mod='addtofreedelivery'}
			</label>
			<div class="col-lg-9">
				<input type="text" name="margin_price_percent" id="margin_price_percent" value="{if isset($margin_price_percent)}{$margin_price_percent}{/if}" class="">
			</div>
		</div>

		<div class="panel-footer" id="toolbar-footer">
			<button class="btn btn-default pull-right" name="saveAddTofreeDelivery" type="submit"><i class="process-icon-save"></i> <span>{l s='Save' mod='addtofreedelivery'}</span></button>
			<a class="btn btn-default" href="{$current_url}">
				<i class="process-icon-cancel"></i> <span>{l s='Cancel' mod='addtofreedelivery'}</span>
			</a>
		</div>
	</form>
</div>