{if isset($priceToFreeDelivery) && $priceToFreeDelivery}
    <div style="margin-bottom:5px;float:left;width:100%">{l s='For free delivery missing:' mod='addtofreedelivery'}
        <span style="font-size: 18px;font-weight: bold;"> {$priceToFreeDelivery} </span>
        {l s='Choose products for your order to receive free delivery:' mod='addtofreedelivery'}
    </div>
{/if}

{if isset($freeShippingProducts) && $freeShippingProducts}
    {include file="$tpl_dir./product-list.tpl" products=$freeShippingProducts class='add-to-free-shipping-product-list' id='add-to-free-shipping'}
{/if}