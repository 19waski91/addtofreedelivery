<?php
class FreeShippingProducts
{
    private $products = [];
    private $limit = 20;
    private $offset = 0;


    public function getTotalFreeShipping()
    {
        $cart = Context::getContext()->cart;
        $total_free_shipping = 0;
        if ($free_shipping = Tools::convertPrice(floatval(Configuration::get('PS_SHIPPING_FREE_PRICE')), Context::getContext()->currency))
        {
            $total_free_shipping =  floatval($free_shipping - ($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS) -
                    $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS)));

            if ($total_free_shipping < 0)
                $total_free_shipping = 0;
        }

        return $total_free_shipping;
    }

    public function getProducts($totalFreeShippingMin, $freeShippingWithPercent, $quantity, $categoriesId, $productsId)
    {
        if($freeShippingWithPercent <= 0){
            return [];
        }
        do{
            $products = self::_getProducts($this->offset, $categoriesId, $productsId);
            $this->offset += $this->limit;
            $this->checkProducts($products, $quantity, $totalFreeShippingMin, $freeShippingWithPercent);
            $break = (count($products) < $this->limit || count($this->products) >= $quantity);
        }while(!$break);

        $this->products = $this->getProductsDetails();
        return $this->products;
    }

    private  function _getProducts($offset, $categoriesId = [], $productsId = [])
    {
        $where = '';
        $sql = new DbQuery();
        $sql->select('p.id_product, p.unit_price_ratio, is.id_image, p.show_price, p.available_for_order, p.customizable, p.minimal_quantity, pl.description_short , pl.link_rewrite, p.id_category_default, p.out_of_stock, p.ean13, pa.id_product_attribute, pa.default_on, pl.name, sa.quantity');
        $sql->from('product', 'p');
        $sql->innerJoin('product_lang', 'pl', 'p.id_product = pl.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id);
        $sql->leftJoin('product_attribute', 'pa', 'p.id_product = pa.id_product');
        $sql->leftJoin('stock_available', 'sa', 'sa.id_product = p.id_product AND sa.id_product_attribute = 0');
        $sql->leftJoin('category_product', 'cp', 'cp.id_product = p.id_product');
        $sql->leftJoin('image_shop', 'is', 'is.id_product = p.id_product AND is.cover=1 AND is.id_shop='.(int)Context::getContext()->shop->id.'');
        $sql->where('sa.quantity > 0');
        $sql->where('p.cache_is_pack = 0');
        if (isset($categoriesId) && !empty($categoriesId)){
            $where .= 'cp.id_category IN ('.implode(',', array_map('intval', $categoriesId)).')';
        }

        if (isset($productsId) && !empty($productsId)){
            $where .= ' OR p.id_product IN ('.implode(',', array_map('intval', $productsId)).')';
        }
        $sql->where($where);

        $sql->orderBy('p.quantity DESC, pa.default_on DESC');
        $sql->limit($this->limit, $offset);
        return Db::getInstance()->executeS($sql);
    }

    private function checkProducts(array $products, $quantity, $totalFreeShippingMin, $freeShippingWithPercent)
    {
        if(empty($products)){
            return false;
        }

        foreach ($products as $product){
            $price = Product::getPriceStatic($product['id_product'], true, $product['id_product_attribute']);
            if ($price >= $totalFreeShippingMin  && $price <= $freeShippingWithPercent){
                $this->checkExistingProducts($product, $price, $quantity);
                if (count($this->products) >= $quantity) {
                    break;
                }
            }
        }
    }

    private function checkExistingProducts(array $product, $price, $quantity)
    {
        if (StockAvailable::getQuantityAvailableByProduct($product['id_product'], $product['id_product_attribute']) <= 0) {
            return false;
        }

        $add = true;
        foreach ($this->products as $key => $chosenProduct){
            if($chosenProduct['id_product'] == $product['id_product']){
                $add &= ($chosenProduct['price'] != $price);
            }
        }
        if($add && count($this->products) <= $quantity){
            $product['price'] = $price;
            $product['id_product_attribute'] = (int)$product['id_product_attribute'];
            $this->products[] = $product;
        }
    }

    private function getProductsDetails()
    {
        if(empty($this->products)){
            return false;
        }

        foreach ($this->products as $key => $product) {
            $array_result[] = ProductCore::getProductProperties(Context::getContext()->language->id, $product);
        }

        return  $array_result;
    }
}
